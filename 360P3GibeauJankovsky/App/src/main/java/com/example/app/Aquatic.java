package com.example.app;

public final class Aquatic extends Animal
{
    protected String hab;

	public Aquatic ()
	{
	    hab = "";
	}
	
	public Aquatic (Aquatic A)
	{
		hab = A.hab;
	}

    @Override
    public String getHab ()
    {
        return hab;
    }

    public void setHab (String T)
    {
        hab = T;
    }
}
