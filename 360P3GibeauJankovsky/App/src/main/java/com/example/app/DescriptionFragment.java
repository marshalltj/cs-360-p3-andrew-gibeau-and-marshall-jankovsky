package com.example.app;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by student on 11/20/13.
 */
public class DescriptionFragment extends Fragment {
    private String aniType;
    private String name; private String info; private String lifespan;
    private String age; private String weight; private String habitat;
    private String continent; private String wingspan;
    private TextView itemTextView;

    public DescriptionFragment(Animal A) {
        this.aniType = A.getType();
        this.name = A.getName(); this.info = A.getDesc(); this.lifespan = A.getLs();
        this.age = A.getAge(); this.weight = A.getWeight();
        if (aniType.equals("Aquatic"))
            this.habitat = A.getHab();
        if (aniType.equals("Land"))
            this.continent = A.getCon();
        if (aniType.equals("Flying")){
            this.wingspan = A.getWs();
            this.continent = A.getCon();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.description_view_fragment_layout, container, false);

        itemTextView = (TextView) view.findViewById(R.id.descriptionTextView);
        itemTextView.setTextSize(12);
        itemTextView.setText("Information:" + '\n' + info);

        itemTextView = (TextView) view.findViewById(R.id.habitatView);
        itemTextView.setTextSize(12);
        if (aniType.equals("Aquatic"))
            itemTextView.setText("\nHabitat: " + habitat);
        else
            itemTextView.setText("\nContinent Found: " + continent);

        itemTextView = (TextView) view.findViewById(R.id.lifespanView);
        itemTextView.setTextSize(12);
        itemTextView.setText('\n' + "Average Lifespan: " + lifespan);

        itemTextView = (TextView) view.findViewById(R.id.ageView);
        itemTextView.setTextSize(12);
        itemTextView.setText('\n' + "Age: " + age);

        itemTextView = (TextView) view.findViewById(R.id.weightView);
        itemTextView.setTextSize(12);
        itemTextView.setText('\n' + "Weight: " + weight);

        if (aniType.equals("Flying"))
        {
            itemTextView = (TextView) view.findViewById(R.id.wingView);
            itemTextView.setTextSize(12);
            itemTextView.setText('\n' + "Wingspan: " + wingspan);
        }

        return view;
    }
}