package com.example.app;

public final class Flying extends Land
{
    protected String ws;

	public Flying ()
	{
	    ws = "";
	}
	
	public Flying (Flying A)
	{
		ws = A.ws;
	}

    @Override
    public String getWs ()
    {
        return ws;
    }

    public void setWs (String T)
    {
        ws = T;
    }
}
